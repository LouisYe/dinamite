#!/bin/bash
clang++ -emit-llvm main.cpp -c -g -o main.bc
opt -load ../../../Release+Asserts/lib/AccessInstrument.so -accessinst -f < main.bc > new.bc
llvm-dis < main.bc > original.txt
llvm-dis < new.bc > new.txt
#lli new.bc > trace.log
llc -filetype=obj new.bc -o new.o
g++ -O3 -o test_instrumented new.o -L./library -linstrumentation 
ldd test_instrumented

#diff original.txt new.txt
